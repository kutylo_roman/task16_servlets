package com.kutylo.domain;

public class Pizza {
    private static int count;
    private int id;
    private String name;
    private String description;

    public Pizza() {
        count++;
        id = count;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
