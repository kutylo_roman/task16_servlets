package com.kutylo.servet;

import com.kutylo.domain.Company;
import com.kutylo.domain.Pizza;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;

public class ListServlet extends HttpServlet {
    protected static Company company = new Company();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        resp.setContentType("text/html");
        PrintWriter out = resp.getWriter();
        out.println("<html>" +
                "<body>");
        out.println("<p>List of pizza:</p>");
        LinkedList<Pizza> list = (LinkedList<Pizza>) company.getPizzaList();
        ;
        for (Pizza pizza : list) {
            out.println("<p>" + "ID: " + pizza.getId()
                    + " name = " + pizza.getName()
                    + "</p>");
        }

        out.println("<form>" +
                "<p><b>Delete</b></p>" +
                "<p> Pizza id: <input type='text' name='pizza_id'>\n" +
                "<input type='button' onclick='remove(this.form.pizza_id.value)' name='ok' value='delete'>" +
                "</p>\n" +
                "</form>");
        out.println("<script type='text/javascript'>\n" +
                " function remove(id) { fetch('pizzas/' + id, {method: 'DELETE'}); }\n" +
                "</script>");

        out.println("<p> <a href='/task16_servlets-1.0-SNAPSHOT'>To Back</a> </p>");
        out.println("</body>" +
                "</html>");
    }

    @Override
    protected void doDelete(HttpServletRequest req, HttpServletResponse resp) {
        String id = req.getRequestURI();
        id = id.replace("/task16_servlets-1.0-SNAPSHOT/pizzas/", " ");
        company.deletePizza(Integer.parseInt(id));
    }

}
